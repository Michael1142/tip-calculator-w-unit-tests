﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculating_Percentages
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.Write("Please enter the bill amount:");
            var preTotal = Convert.ToDecimal(Console.ReadLine());

            Console.Write("Please enter the tip percentage:");
            var percentage = Convert.ToDecimal(Console.ReadLine());

            decimal tipAmount = CalTip(preTotal, percentage);

            Console.WriteLine($"The tip is: £{tipAmount}");

            decimal totalWTip = CalTotal(preTotal, tipAmount);

            Console.WriteLine($"The total tip amount is: £{totalWTip}");

            Console.ReadKey();
        }

        public static decimal CalTotal(decimal preTotal, decimal tipAmount)
        {
            var calcValue = decimal.Round(preTotal + tipAmount, 2, MidpointRounding.AwayFromZero);
            decimal decimalRounded = Decimal.Parse(calcValue.ToString("0.00"));
            return decimalRounded;
        }

        public static decimal CalTip(decimal total, decimal percentage)
        {
            decimal totalTip;
            totalTip = total * percentage / 100;
            totalTip = decimal.Round(totalTip, 2, MidpointRounding.AwayFromZero);
            decimal decimalRounded = Decimal.Parse(totalTip.ToString("0.00"));
            return decimalRounded;
        }
    }
}
