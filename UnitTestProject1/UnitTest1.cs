﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void WholeValuesTip()
        {
            decimal amountIn = 12.00M;
            decimal tip = 15;
            decimal answer = 1.80M;

            Assert.AreEqual(Calculating_Percentages.Program.CalTip(amountIn, tip), answer);
        }

        [TestMethod]
        public void WholeValuesTotal()
        {
            decimal amountIn = 12.00M;
            decimal tipValue = 2.07M;
            decimal answer = 14.07M;

            Assert.AreEqual(Calculating_Percentages.Program.CalTotal(amountIn, tipValue), answer);
        }

        [TestMethod]
        public void RoundedValueTip()
        {
            decimal amountIn = 12.35M;
            decimal tip = 15;
            decimal answer = 1.85M;

            Assert.AreEqual(Calculating_Percentages.Program.CalTip(amountIn, tip), answer);
        }

        [TestMethod]
        public void RoundedValueTotal()
        {
            decimal amountIn = 12.35M;
            decimal tipValue = 1.85M;
            decimal answer = 14.20M;

            Assert.AreEqual(Calculating_Percentages.Program.CalTotal(amountIn, tipValue), answer);
        }

        [TestMethod]
        public void ZeroValueTip()
        {
            decimal amountIn = 0.00M;
            decimal tip = 15;
            decimal answer = 0.00M;
                
            Assert.AreEqual(Calculating_Percentages.Program.CalTip(amountIn, tip), answer);
        }

        [TestMethod]
        public void ZeroValueTotal()
        {
            decimal amountIn = 0.00M;
            decimal tip = 0.00M;
            decimal answer = 0.00M;

            Assert.AreEqual(Calculating_Percentages.Program.CalTotal(amountIn, tip), answer);
        }
    }
}
